import axios, { AxiosError } from "axios";
import { config } from "../config";
import { chunk } from '../common';

const baseUrl = "https://api.vtg.nexiot.ch/v1";
const groupId = "TtNgil";
const resourceTypes = ['asset', 'device', 'geofence', 'trigger', 'user'];
(async () => {
  try {
    const auth = {
      username: config.username,
      password: config.apiKey,
    };
    const response = await axios.get(
      `${baseUrl}/groups/members/${groupId}?resource_types=Geofence,ExtDevice,Asset,Trigger,Device,User`,
      { auth }
    );
    for (const type of resourceTypes) {
      console.log(`${response.data[type+'_ids'].length} ${type}s`);
    }
    let count = 0;
    const step = 300;
    for (const type of resourceTypes) {
      console.log(`Removing ${type}s...\n`);
      const resourceIds = response.data[`${type}_ids`];
      for (const ids of chunk(resourceIds, step)) {
          await axios.patch(`${baseUrl}/groups/members/${groupId}`, { remove: { [type+'_ids']: ids }, add: {} }, { auth });
          count += ids.length;
          console.log(`Removed ${count} ${type}s from group`);
      }
    }
  } catch (e) {
    console.error((e as AxiosError)?.response?.data || e.message);
  }
})();
