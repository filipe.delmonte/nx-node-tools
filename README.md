# Just some scripts for adhoc stuff people ask me to do...

## Get started
```
# create file ./config/index.ts with the following content
export const config = {
    username: '',
    apiKey: '',
};
```

## Usage
```
$ ts-node ./command/<whatever script you want>.ts
```

